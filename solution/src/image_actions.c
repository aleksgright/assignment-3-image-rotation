#include "./include/image_actions.h"

#include <malloc.h>
#include <stdint.h>

size_t get_padding(uint64_t width) {
    return (size_t) OFFSET - (width * PIXEL_STRUCT_SIZE) % OFFSET;
}

void free_image(struct image* image) {
    free(image->data);
    free(image);
}
