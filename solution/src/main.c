#include "./include/bmp_reader.h"
#include "./include/bmp_writer.h"
#include "./include/image_actions.h"
#include "./include/rotate.h"
#include "./include/struct_image.h"

#include <malloc.h>
#include <stdio.h>


int main(int argc, char **argv) {
    if (argc != 3) {
        fprintf(stderr, "%s\n", "Invalid number of arguments");
        return 1;
    }

    FILE *source_file = fopen(argv[1], "rb");
    if (source_file == NULL) {
        fprintf(stderr, "Invalid input file");
    }

    FILE *target_file = fopen(argv[2], "wb ");
    if (source_file == NULL) {
        fprintf(stderr, "Invalid output file");
    }

    struct image *source_image = malloc(sizeof(struct image));
    struct image *target_image = malloc(sizeof(struct image));
    if (from_bmp(source_file, source_image) == READ_OK) {
        rotate_image(source_image, target_image);
        to_bmp(target_file, target_image);
    }
    free_image(target_image);
    free_image(source_image);
    return 0;
}
