#ifndef STRUCT_IMAGE
#define STRUCT_IMAGE

#include <stdint.h>

struct image {
    uint64_t width, height;
    struct pixel *data;
};

struct pixel {
    uint8_t r, g, b;
};

#endif
