#ifndef IMAGE_ACTIONS
#define IMAGE_ACTIONS

#include "struct_image.h"

#include <stddef.h>

#define PIXEL_STRUCT_SIZE 3
#define OFFSET 4

size_t get_padding(size_t width);
void free_image(struct image* image);

#endif
