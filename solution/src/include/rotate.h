#ifndef ROTATE
#define ROTATE

#include "struct_image.h"

struct image* rotate_image(struct image* const source, struct image* target);

#endif
